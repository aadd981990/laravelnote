<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NotesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/notes',[NotesController::class,'show'])->name('show');
Route::get('/note/{note}',[NotesController::class,'note'])->name('note');
Route::any('/note/{note}/edit',[NotesController::class,'edit_note'])->name('note.edit');
Route::get('/delete_note/{note}',[NotesController::class,'delete_note'])->name('note.delete');
Route::any('/',[NotesController::class,'newnote'])->name('newnote');
Route::any('/newnote',[NotesController::class,'newnote'])->name('newnote');
Route::any('/search',[NotesController::class,'search'])->name('search');
