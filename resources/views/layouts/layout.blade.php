<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
    <style>
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('newnote')}}">Νεα σημείωση   <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{route('show')}}">Σημειώσεις  <span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method action="{{route('search')}}">
          <input class="form-control mr-sm-2" type="search" name="q" placeholder="Αναζήτηση" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Αναζήτηση</button>
        </form>
      </div>
    </nav>
    
    @yield('content')

  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/validation.js" type="text/javascript"></script>
</body>
</html>