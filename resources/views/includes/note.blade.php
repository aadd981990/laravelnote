<div class="col-md-4 space" >
    <div class="card">
        <div class="card-body ">
            <a href="{{route('note',$note)}}">  <h5 class="card-title">{{ $note->title }}</h5></a>
            <p class="card-text">{{$note->body}}</p>
            <a href="{{route('note.edit',$note)}}" class="btn btn-primary">Επεξεργασία</a>
            <a href="{{route('note.delete',$note)}}"  class="btn btn-danger deleteButton">Διαγραφή</a>
        </div>
    </div>
</div>