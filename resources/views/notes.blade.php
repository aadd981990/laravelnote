@extends('layouts.layout')
@section('content')
<div class="container">
    @forelse($notes as $note)
        <div class="row">
            @include('includes.note')
        </div>
    @empty
        <div class="row">
            <div class="col-12 text-center">
                <h1>Δεν βρέθηκαν σημειώσεις.</h1>
            </div>
        </div>   
    @endforelse
</div>
@endsection