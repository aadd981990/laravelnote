@extends('layouts.layout');

@section('content')
    <div class="container">
        <form action="" method="POST">
            @csrf
            <div class="container">
                    <div class="row d-flex justify-content-center space" >
                        <input type="text" style=" width: 50%;" id="title"  name="title" placeholder="Δώστε τίτλο" class="form-control" value="{{ $note->title }}">
                    </div>
                    <div class="row d-flex justify-content-center space">
                        <textarea id="body" name="body" cols="30" rows="10" style="width: 50%;" placeholder="Πληκτρολογήστε την σημείωσή σας ">{{ $note->body }}</textarea> 
                    </div>
                    <div class="row d-flex justify-content-center space" >
                        <button id="ButtonSaveNew" style=" width: 50%;" class="btn btn-primary">Αποθήκευση</button>
                    </div>
            </div>
        </form>
    </div>
@endsection
 