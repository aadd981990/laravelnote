@extends('layouts.layout')

@section('content')
<div class="container card">
    <div class="row">
        <div class="card-body">
            <h1 class="card-title">{{ $note->title }}</h1>
            <p class="card-text">{{$note->body}}</p>
            <a href="{{route('note.edit',$note)}}" class="btn btn-primary">Eπεξεργασία</a>
            <a href="{{route('note.delete',$note)}}" class="deleteButton" class="btn btn-danger">Διαφραφή</a> 
        </div>
    </div>
</div>
@endsection

