@extends('layouts.layout')
@section('content')
    <div class="container">
        <form action="" method="POST">
            @csrf
            <div class="container">
                    <div class="row d-flex justify-content-center space" >
                        <input id="title" type="text" name="title" placeholder="Δώστε τίτλο" style=" width: 50%;" class="form-control">
                    </div>
                    <div class="row d-flex justify-content-center space">
                        <textarea id="body" class="form-control" name="body" cols="30" rows="10"  style="width: 50%;" placeholder="Πληκτρολογήστε την σημείωσή σας "></textarea> 
                    </div>
                    <div class="row d-flex justify-content-center space" >
                        <button id="ButtonSaveNew" style=" width: 50%;" class="btn btn-primary">Αποθήκευση</button>
                    </div>
            </div>
        </form>
    </div>
@endsection