<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
class NotesController extends Controller
{
    public function show()
    {
        $notes=Note::All()->sortDesc();
        return view('notes',['notes'=>$notes]);
    }

    public function note(Note $note)
    {
        return view('note',['note'=>$note]);
    }

    public function edit_note(Note $note, Request $request)
    {
        if($request->method()=="POST"){ 
           $note->title = $request->get('title');
           $note->body = $request->get('body');
           if($note->save()){
            return redirect(route('show'));
           };
        }
        return view('edit_note',['note'=>$note]);
    }

    public function newnote(Request $request)
    {
        if($request->method()=="POST"){
           $note = new Note();
           $note->title = $request->get('title');
           $note->body = $request->get('body');
           if($note->save()){
                return redirect(route('show'));
           }
        }
       return view('newnote'); 
    }

    public function search(Request $request)
    {
        $notes = [];
        if ($request->filled('q')){  
            $q = $request->get('q');
            $notes=Note::where('title', 'like', '%'.$q.'%')->get();
        }
        return view('notes',['notes' => $notes]);
    }

    public function delete_note(Note $note)
    {
        $note->delete();
        return redirect('notes');
    }  
}
