$( document ).ready(function() {
    var title = 0;
    var body = 0;
    $(function(){
        $('#ButtonSaveNew').prop('disabled', true);
    });
    $("#title").on("propertychange change keyup paste input", function(){
        title = $('#title').val().length;
        checkInput(title,body);
    });
    $("#body").on("propertychange change keyup paste input", function(){
        body = $('#body').val().length;
        checkInput(title,body);   
    });
    
    function checkInput(title,body){
        if(title > 0 && body > 0){
            $('#ButtonSaveNew').prop('disabled', false);
        }else{
            $('#ButtonSaveNew').prop('disabled', true);
        }  
    } 
    
});
